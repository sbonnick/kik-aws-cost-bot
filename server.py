
import os
import os.path


from flask import Flask, request, Response
from kik import KikApi, Configuration
from kik.messages import messages_from_json, TextMessage

import costs as costs

REGION = "US West (N. California)"

APP = Flask(__name__)

PORT = int(os.environ['KIK_PORT'] or 8080)
BOT = os.environ['KIK_BOT']
API_KEY = os.environ['KIK_KEY']
WEBHOOK = "{hook}:{port}/cost".format(hook=os.environ['KIK_HOOK'], port=str(PORT))

KIK = KikApi(BOT, API_KEY)
KIK.set_configuration(Configuration(webhook=WEBHOOK))

COST_SHEET = costs.load_cost_sheet()

@APP.route('/cost', methods=['POST'])
def incoming():
    '''MVP: Recive chat messages from Kik and returns a meesage with AWS EC2 costs'''
    if not KIK.verify_signature(request.headers.get('X-Kik-Signature'), request.get_data()):
        return Response(status=403)

    messages = messages_from_json(request.json['messages'])

    for message in messages:
        if isinstance(message, TextMessage):
            image_list = message.body.splitlines(True)
            cost_list = costs.append_costs(COST_SHEET, REGION, image_list)

            #sending back as single message to avoid dealing with the "max of 5" restriction
            KIK.send_messages([
                TextMessage(
                    to=message.from_user,
                    chat_id=message.chat_id,
                    body="\n".join(cost_list)
                )
            ])

    return Response(status=200)


if __name__ == "__main__":
    APP.run(port=PORT, debug=True, host='0.0.0.0')
