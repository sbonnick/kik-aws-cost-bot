
import os.path
import json
import requests

COST_ROUTE = 'https://pricing.us-east-1.amazonaws.com/offers/v1.0/aws/AmazonEC2/current/index.json'


def load_cost_sheet():
    ''' MVP parsing of the AWS price list. It needs much better error handling '''
    if os.path.isfile('ec2_costs.json'):
        with open('ec2_costs.json', 'r') as json_file:
            return json.load(json_file)

    ec2_costs = requests.get(COST_ROUTE).json()

    cost_sheet = {}
    for item in ec2_costs['products']:
        try:
            instance = ec2_costs['products'][item]['attributes']['instanceType']
            region = ec2_costs['products'][item]['attributes']['location']

            first_cost_obj = next(iter(ec2_costs['terms']['OnDemand'][item].values()))
            first_cost_dim = next(iter(first_cost_obj['priceDimensions'].values()))

            if instance not in cost_sheet:
                cost_sheet[instance] = {}

            cost_sheet[instance][region] = first_cost_dim['pricePerUnit']['USD']

        except: 
            pass

    with open('ec2_costs.json', 'w') as json_file:
        json.dump(cost_sheet, json_file, indent=4, sort_keys=True)

    return cost_sheet


def append_costs(cost_sheet, region, image_list):
    ''' returns a modified list that includes the costs for each item'''
    cost_list = []
    for image in image_list:
        trimmed_image = image.strip().lower()

        if trimmed_image in cost_sheet and region in cost_sheet[trimmed_image]:
            cost = "$" + cost_sheet[trimmed_image][region].rstrip("0")
        else:
            cost = "NA"

        cost_list.append("{image} = {cost}".format(image=trimmed_image, cost=cost))
    return cost_list
