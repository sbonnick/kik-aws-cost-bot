# KIK AWS Cost Bot
The is the server side code to support a KIK bot which when queried with
an EC2 instance or list of instances (one per line) it will return the costs
the instances. Currently, as an MVP, queries are made against the 
California region and there are is a lot of missing error handling code.

## Running the Server
In windows, with python3 install, run the following (substituting env where needed):
```
set KIK_PORT=8080
set KIK_BOT=<bot_name>
set KIK_KEY=<bot_api_key>
set KIK_HOOK=http://example.com

py -3 server.py
```
This will run the server on the give port and provide the KIK bot with the given 
KIK_Hook. this hook needs to map back to this server.
